//
//  UIDynamicAnimatorViewController.swift
//  Animations
//
//  Created by GABELEV Evgeny on 14.01.2020.
//  Copyright © 2020 Evgeny Gabelev. All rights reserved.
//

import UIKit

class UIDynamicAnimatorViewController: UIViewController {
    let cardView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.backgroundColor = .red
        return view
    }()
    
//    var animator: UIDynamicAnimator!
//    var snapping: UISnapBehavior!
//    var gravity: UIGravityBehavior!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        cardView.center = view.center
        view.addSubview(cardView)
        
//        animator = UIDynamicAnimator(referenceView: view)
//        snapping = UISnapBehavior(item: cardView, snapTo: view.center)
//        //snapping.damping
//        animator.addBehavior(snapping)
        
//        gravity = UIGravityBehavior(items: [cardView])
//        animator.addBehavior(gravity)
        
//        let collision = UICollisionBehavior(items: [cardView])
//        collision.translatesReferenceBoundsIntoBoundary = true
//        animator.addBehavior(collision)
        
//        let behavior = UIDynamicItemBehavior(items: [cardView])
//        behavior.elasticity = 0.6
//        animator.addBehavior(behavior)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panView(recognizer:)))
        cardView.addGestureRecognizer(panGesture)
        cardView.isUserInteractionEnabled = true
    }
    
    @objc func panView(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case.changed:
            let translation = recognizer.translation(in: view)
            cardView.center = CGPoint(x: cardView.center.x + translation.x,
                                      y: cardView.center.y + translation.y)
            recognizer.setTranslation(.zero, in: view)
//        case .began:
//            animator.removeBehavior(snapping)
//        case .ended, .cancelled, .failed:
//            animator.addBehavior(snapping)
        default:
            break
        }
    }
}
