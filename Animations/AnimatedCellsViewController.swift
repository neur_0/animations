//
//  AnimatedCellsViewController.swift
//  Animations
//
//  Created by Evgeny Gabelev on 01.01.2020.
//  Copyright © 2020 Evgeny Gabelev. All rights reserved.
//

import UIKit

class AnimatedCellsViewController: UIViewController {
    let tableView = UITableView()
    var cellsData: [String] = []
    
    private var safeAreaTop: CGFloat {
        return navigationController?.navigationBar.frame.maxY ?? 64
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: safeAreaTop),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        generateCellsData()
        animateTable()
    }
    
    func generateCellsData() {
        for index in 0...25 {
            cellsData.append("cell \(index)")
        }
    }
    
    func animateTable() {
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableHeight: CGFloat = tableView.bounds.size.height
        
        for cellItem in cells {
            let cell = cellItem as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for cellItem in cells {
            let cell = cellItem as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension AnimatedCellsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        cell.textLabel?.text = cellsData[indexPath.row]
        return cell
    }
}

extension AnimatedCellsViewController: UITableViewDelegate {
    
}
