//
//  Demo3ViewController.swift
//  Animations
//
//  Created by GABELEV Evgeny on 27.12.2019.
//  Copyright © 2019 Evgeny Gabelev. All rights reserved.
//

import UIKit

class Demo3ViewController: UIViewController {
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.image = UIImage(named: "apple")
        return imageView
    }()
    
    let slider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.heightAnchor.constraint(equalToConstant: 35).isActive = true
        slider.widthAnchor.constraint(equalToConstant: 250).isActive = true
        slider.minimumValue = 0
        slider.maximumValue = 1
        slider.value = 0
        slider.minimumTrackTintColor = .lightGray
        slider.maximumTrackTintColor = .darkGray
        slider.thumbTintColor = .black
        slider.addTarget(self, action: #selector(changeValue(sender:)), for: .valueChanged)
        return slider
    }()
    
    let animator = UIViewPropertyAnimator(duration: 1.0, curve: .linear)
    
    var constraintX: NSLayoutConstraint?
    var constraintY: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintX = NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        constraintY = NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
        view.backgroundColor = .white
        view.addSubview(imageView)
        view.addConstraint(constraintX!)
        view.addConstraint(constraintY!)
        imageView.transform = CGAffineTransform(scaleX: 1, y: 1)
        
        view.addSubview(slider)
        slider.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        slider.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        
        createAnimation()
    }
    
    @objc func changeValue(sender: UISlider) {
        animator.fractionComplete = CGFloat(sender.value)
    }
    
    func createAnimation() {
        animator.addAnimations {
            self.imageView.transform = CGAffineTransform(scaleX: 3, y: 3)
        }
        
        animator.addAnimations({
            self.constraintY?.constant = -200
            self.imageView.alpha = 0
            self.view.layoutIfNeeded()
        }, delayFactor: 0.5)
    }
    
    deinit {
        animator.stopAnimation(true)
    }
}
