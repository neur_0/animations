//
//  Demo1ViewController.swift
//  Animations
//
//  Created by GABELEV Evgeny on 14/12/2019.
//  Copyright © 2019 Evgeny Gabelev. All rights reserved.
//

import UIKit

class Demo1ViewController: UIViewController {
    lazy var redSquare: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: safeAreaTop, width: 100, height: 100))
        view.backgroundColor = .red
        return view
    }()
    
    private var safeAreaTop: CGFloat {
        return navigationController?.navigationBar.frame.maxY ?? 64
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(redSquare)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        singleAnimation()
        //sequenceAnimation()
        //springAnimation()
        //transformAnimation()
        //keyFrameAnimation()
    }
    
    func singleAnimation() {
        UIView.animate(withDuration: 3) {
            self.redSquare.center = self.view.center
            self.redSquare.backgroundColor = .blue
        }
    }
    
    func sequenceAnimation() {
        UIView.animate(withDuration: 2, animations: {
            self.redSquare.center = self.view.center
        }) { _ in
            UIView.animate(withDuration: 2, animations: {
                self.redSquare.backgroundColor = .blue
            }) { _ in
                UIView.animate(withDuration: 5) {
                    self.redSquare.layer.masksToBounds = true
                    self.redSquare.layer.cornerRadius = 20
                }
            }
        }
    }
    
    func springAnimation() {
        UIView.animate(withDuration: 2, delay: 1, usingSpringWithDamping: 0.3, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
        self.redSquare.center = self.view.center
        }, completion: nil)
    }
    
    func transformAnimation() {
        let rotate = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        let scale = CGAffineTransform(scaleX: 1, y: 2)
        let translate = CGAffineTransform(translationX: 200, y: 200)
        let transform = rotate
        //let transform = rotate.concatenating(translate)
        
        UIView.animate(withDuration: 2, animations: {
            self.redSquare.transform = transform
        })
    }
    
    func keyFrameAnimation() {
        UIView.animateKeyframes(withDuration: 3.0, delay: 0.0, options: [.repeat, .autoreverse], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
                self.redSquare.center = self.view.center
            }

            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.redSquare.transform = CGAffineTransform(scaleX: 2, y: 1)
            }
        }, completion: nil)
    }
    
}
