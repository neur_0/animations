//
//  Demo4ViewController.swift
//  Animations
//
//  Created by Evgeny Gabelev on 06.01.2020.
//  Copyright © 2020 Evgeny Gabelev. All rights reserved.
//

import UIKit

class Demo4ViewController: UIViewController {
    let circleLayer = CALayer()
    var appearedOnce = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        initCircleLayer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //basicCAAnimation()
        keyFrameAnimation()
        //groupedAnimation()
//        if !appearedOnce {
//            cardTransition()
//        }
    }
    
    func initCircleLayer() {
        circleLayer.bounds = CGRect(x: 0, y: 0, width: 50, height: 50)
        circleLayer.position = CGPoint(x: 50, y: 120)
        circleLayer.backgroundColor = UIColor.red.cgColor
        circleLayer.cornerRadius = circleLayer.bounds.width / 2
        view.layer.addSublayer(circleLayer)
    }
    
    func basicCAAnimation() {
        circleLayer.removeAllAnimations()
        let animation = CABasicAnimation(keyPath: "position") // из документации
        let startPoint = NSValue(cgPoint: CGPoint(x: 50, y: 120))
        let endPoint = NSValue(cgPoint: CGPoint(x: 400, y: 120))
        animation.fromValue = startPoint // fromValue и toValue можно заменить на byValue
        animation.toValue = endPoint
        animation.repeatCount = Float.greatestFiniteMagnitude
        animation.duration = 2.0
        animation.timingFunction = .init(name: .easeInEaseOut)
        //animation.timingFunction = .init(controlPoints: 0.5, 0, 0.9, 0.7) // кастомная функция, значения могут быть отрицательными
        circleLayer.add(animation, forKey: "linearMovement") // любые
    }
    
    func keyFrameAnimation() {
        circleLayer.removeAllAnimations()
        
        let animation = CAKeyframeAnimation(keyPath: "position.x")
        animation.values = [50, 60, 40, 60, 50]
        animation.keyTimes = [0, 0.25, 0.5, 0.75, 1]
        animation.duration = 0.5
        animation.repeatCount = Float.greatestFiniteMagnitude
        //animation.timingFunction
        //animation.timingFunctions = [CAMediaTimingFunction]
        circleLayer.add(animation, forKey: "shake")
    }
    
    func groupedAnimation() {
        circleLayer.removeAllAnimations()
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 280, y: 100))
        path.addCurve(to: CGPoint(x: 320, y: 100), control1: CGPoint(x: 100, y: 400), control2: CGPoint(x: 500, y: 400))
        path.addCurve(to: CGPoint(x: 280, y: 100), control1: CGPoint(x: 600, y: 500), control2: CGPoint(x: 0, y: 500))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path
        shapeLayer.lineWidth = 3.0
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.black.cgColor
        view.layer.addSublayer(shapeLayer)
        
        let positionAnimation = CAKeyframeAnimation(keyPath: "position")
        positionAnimation.calculationMode = .linear
        positionAnimation.path = path
        
        let colorAnimation = CAKeyframeAnimation(keyPath: "backgroundColor")
        let colors = [UIColor.red.cgColor, UIColor.blue.cgColor, UIColor.green.cgColor]
        colorAnimation.values = colors
        colorAnimation.calculationMode = .paced
        
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [positionAnimation, colorAnimation]
        groupAnimation.duration = 5.0
        groupAnimation.repeatCount = Float.greatestFiniteMagnitude
        groupAnimation.autoreverses = true
        circleLayer.add(groupAnimation, forKey: "multiAnimation")
    }
    
    func cardTransition() {
        appearedOnce = true
        let transition = CATransition()
        transition.duration = 2.0
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .fade
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.pushViewController(SecondViewController(), animated: false) //must be false for custom transition
    }
}
