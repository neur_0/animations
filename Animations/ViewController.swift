//
//  ViewController.swift
//  Animations
//
//  Created by Evgeny Gabelev on 09.12.2019.
//  Copyright © 2019 Evgeny Gabelev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.alignment = .fill
        stack.spacing = 5
        return stack
    }()
    
    let demo1Button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("Demo 1", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(didTapDemoButton(sender:)), for: .touchUpInside)
        button.tag = 1
        return button
    }()
    
    let demo2Button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("Demo 2", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(didTapDemoButton(sender:)), for: .touchUpInside)
        button.tag = 2
        return button
    }()
    
    let demo3Button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("Demo 3", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(didTapDemoButton(sender:)), for: .touchUpInside)
        button.tag = 3
        return button
    }()
    
    let demo4Button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("Demo 4", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(didTapDemoButton(sender:)), for: .touchUpInside)
        button.tag = 4
        return button
    }()
    
    let animatedCellsButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("Animated cells", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(didTapDemoButton(sender:)), for: .touchUpInside)
        button.tag = 5
        return button
    }()
    
    let dynamicAnimatorButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.setTitle("DynamicAnimator", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.addTarget(self, action: #selector(didTapDemoButton(sender:)), for: .touchUpInside)
        button.tag = 6
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        stackView.addArrangedSubview(demo1Button)
        stackView.addArrangedSubview(demo2Button)
        stackView.addArrangedSubview(demo3Button)
        stackView.addArrangedSubview(demo4Button)
        stackView.addArrangedSubview(animatedCellsButton)
        stackView.addArrangedSubview(dynamicAnimatorButton)
        
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    @objc func didTapDemoButton(sender: UIButton) {
        switch sender.tag {
        case 1:
            navigationController?.pushViewController(Demo1ViewController(), animated: true)
        case 2:
            navigationController?.pushViewController(Demo2ViewController(), animated: true)
        case 3:
            navigationController?.pushViewController(Demo3ViewController(), animated: true)
        case 4:
            navigationController?.pushViewController(Demo4ViewController(), animated: true)
        case 5:
            navigationController?.pushViewController(AnimatedCellsViewController(), animated: true)
        case 6:
            navigationController?.pushViewController(UIDynamicAnimatorViewController(), animated: true)
        default:
            break
        }
    }

}

