//
//  SecondViewController.swift
//  Animations
//
//  Created by GABELEV Evgeny on 01.02.2020.
//  Copyright © 2020 Evgeny Gabelev. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
    }
}
