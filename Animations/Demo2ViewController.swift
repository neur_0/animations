//
//  Demo2ViewController.swift
//  Animations
//
//  Created by GABELEV Evgeny on 24.12.2019.
//  Copyright © 2019 Evgeny Gabelev. All rights reserved.
//

import UIKit

class Demo2ViewController: UIViewController {
    let viewToAdd: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = .yellow
        return view
    }()
    
    let redView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        view.backgroundColor = .red
        return view
    }()
    
    let blueView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        view.backgroundColor = .blue
        return view
    }()
    
    let firstLabel: UILabel = {
        let label = UILabel()
        label.text = "first view"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let secondLabel: UILabel = {
        let label = UILabel()
        label.text = "second view"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let actionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button.setTitle("Perform transition", for: .normal)
        button.backgroundColor = .lightGray
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        return button
    }()
    
    private var isFlipped: Bool = false
    
    fileprivate func initialSetup() {
        view.backgroundColor = .white
        
        view.addSubview(redView)
        redView.center = view.center
        redView.addSubview(secondLabel)
        secondLabel.centerXAnchor.constraint(equalTo: redView.centerXAnchor).isActive = true
        secondLabel.centerYAnchor.constraint(equalTo: redView.centerYAnchor).isActive = true
        
        view.addSubview(blueView)
        blueView.center = view.center
        blueView.addSubview(firstLabel)
        firstLabel.centerXAnchor.constraint(equalTo: blueView.centerXAnchor).isActive = true
        firstLabel.centerYAnchor.constraint(equalTo: blueView.centerYAnchor).isActive = true
        
        view.addSubview(actionButton)
        actionButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        actionButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    @objc func didTapActionButton() {
        transitionWithViewAddition()
        //coloredTransition()
        //flippedTransition()
        //transitionFrom()
    }
    
    func transitionWithViewAddition() {
        redView.isHidden = true
        UIView.transition(with: blueView, duration: 1, options: .transitionFlipFromBottom, animations: {
            self.isFlipped = !self.isFlipped
            _ = self.isFlipped ? self.blueView.addSubview(self.viewToAdd) : self.viewToAdd.removeFromSuperview()
        }, completion: nil)
    }
    
    func coloredTransition() {
        redView.isHidden = true
        isFlipped = !isFlipped
        UIView.transition(with: blueView, duration: 0.5, options: .transitionFlipFromRight, animations: {
            self.blueView.backgroundColor = self.isFlipped ? .red : .blue
        }, completion: nil)
    }
    
    func flippedTransition() {
        isFlipped = !isFlipped
        let cardToFlip = isFlipped ? blueView : redView
        let bottomCard = isFlipped ? redView : blueView
        
        UIView.transition(with: cardToFlip, duration: 1.0, options: [.transitionFlipFromRight], animations: {
            cardToFlip.alpha = 0
        })

        UIView.transition(with: bottomCard, duration: 1.0, options: [.transitionFlipFromRight], animations: {
            bottomCard.alpha = 1
        })
    }
    
    func transitionFrom() {
        isFlipped = !isFlipped
        let cardToFlip = isFlipped ? blueView : redView
        let bottomCard = isFlipped ? redView : blueView
        
        UIView.transition(from: cardToFlip, to: bottomCard, duration: 1, options: [.transitionFlipFromRight, .curveEaseInOut], completion: nil)
    }
}
